#!/usr/bin/env python3

"""
Simple Script Images listed in a csv file: 
---
Example Run: 

./main.py --csv imagefile.csv 

All Errors will be posted to STDOUT. 

Results will be dumped to a file called results.csv
"""

import csv
import sys
from PIL import Image
import imagehash
import time
import logging
from datetime import datetime
import argparse

# Setup Logger
logger = logging.getLogger('__name__')
extensions = "'.jpeg', '.jpg', '.png'"

# Simple function to do the actual compare ...Since no specifics were given I'm just doing a general comparison form this imagehash library
def image_compare(image_one, image_two):
    hash_one = imagehash.average_hash(Image.open(image_one))
    hash_two = imagehash.average_hash(Image.open(image_two))
    results = hash_one - hash_two
    return results


def compare_file(input_file, extensions=extensions):
    logger.info(f'Running Comparison for file {input_file}')
    if not input_file.lower().endswith('.csv'):
        logger.info(f'Incorrect file extension {input_file} - must be csv')
        print('Sorry wrong extension, only CSV files allowed')
        sys.exit(1)
    image_list = []
    try:
        logger.debug(f'Attempting to open file {input_file}')
        with open (input_file, newline='') as csv_file:
            reader = csv.DictReader(csv_file)
            logger.debug('Opened File, running main loop')
            for row in reader:
                image_one = row['image_one']
                image_two = row['image_two']
                # If we don't support the file extension don't check it. 
                if not image_one.lower().endswith(tuple(extensions)) or not image_two.lower().endswith(tuple(extensions)):
                    logger.error(f'file image type not supported {image_one} or {image_two}')
                    pass
                try:
                    logger.debug(f'Running Comparison on Images {image_one} & {image_two}')
                    start_time = time.time()
                    results = image_compare(image_one, image_two)
                    end_time = time.time()
                    logger.debug('Successful Comparison')
                except Exception as err:
                    logger.error(f'run compare error - {err} - Skipping Comparison')
                    continue
                elapsed_time = end_time - start_time
                row = {'image_one': image_one, 'image_two': image_two, 'simuilar': results, 'elapsed': elapsed_time}
                image_list.append(row)
    except Exception as err:
        logger.error(f'Error Opening File: {err}')
        sys.exit(1)
    try:
        with open('results.csv', 'w', newline='') as cfile:
            fields = list(image_list[0].keys())
            writer = csv.DictWriter(cfile, fieldnames=fields)
            writer.writeheader()
            for line in image_list:
                writer.writerow(line)
    except Exception as err:
        logger.error(f'output error - {err}')


# Run the script with the given csv file.
if __name__ == '__main__':
    # Setup Arugments
    parser = argparse.ArgumentParser(description='Simple Image Comparison Script')
    parser.add_argument('--csv', dest='input_file', help='CSV File with Images to Compare', required=True)
    parser.add_argument('--log', dest='log_level', help='Log Level [ex: info,error,debug]', required=False, default='info')
    parser.add_argument('--ext', dest='extensions', help='Image file types [Default support: jpeg, jpg and png]', default=extensions)
    args = parser.parse_args()

    # Setup Logging (Output as logformat)
    log_config = {'debug': logging.DEBUG, 'error': logging.ERROR, 'info': logging.INFO,}
    log_level = log_config[args.log_level.lower()]
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('time=%(asctime)s level=%(levelname)s msg=%(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(log_level)
  

    # Run the main loop
    main(args.input_file, args.extensions)