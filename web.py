#!/usr/bin/env python3

from flask import Flask, flash, request, redirect, url_for, send_file
from werkzeug.utils import secure_filename
import os
import image_check as ic

UPLOAD_FOLDER = ''
ALLOWED_EXTENSIONS = {'csv'}
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def csv_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # Check for file
        if 'file' not in request.files:
            flash('no file')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('no file selected')
            return redirect(request.url)
        if file and csv_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            try:
                # Run main script that does the comparison
                log = ic.compare_file(filename)
                # Return the results as a file called results.csv
                return send_file('results.csv', as_attachment=True)
            except:
                print ('error')
            return redirect(url_for('upload_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''

if __name__ == '__main__':
    app.run(debug=True)