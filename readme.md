# Simple Image Comparison Script

This is a simple script that will compare two images return their hash and then diff that hash to see how different the images are. 

The library used to determine the image differences is: https://pypi.org/project/ImageHash/ 

**Instructions on how to run the script are listed below.**

The following assumptions have been made: 
1. You already know how to use git and clone the repo on to your personal computer. 
2. You are comfortable with the command line enough to open up a terminal and follow instructions. 

If these are incorrect it is recommended to read these guides: 
- using git: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository
- running python: https://www.cs.bu.edu/courses/cs108/guides/runpython.html
- install git : https://git-scm.com/download/linux


## CSV File Format

The header fields must be as below:

| image_one | image_two |
| ---       | ---       |
| aaa.jpeg  | ddd.jpeg  | 

So the file would look like this: 
```
$cat csvfile.csv

image_one,image_two
aaa.jpeg,bbb.jpeg
```

Please supply full path for images! The images directory in the repo can be used if desired, you can simply add the images and add  them to the repo if desired or just keep things local. 

## Install

Follow the steps below to install this script for your operating system.

### Linux/MacOS 
1. git clone the repo! 
2. pip (or pip3) **Make sure python3** run  `pip3 install requirements.txt`
3. Open up your terminal and just run the command  `image_check.py` 


    ```
    ./image_check.py --csv $CSVFILE.csv
    ```

The output will be dumped into a file `results.csv`

### Windows
1. git clone the repo!
2. open powershell or command
3. pip (or pip3) **Make sure python3** run  `pip3 install requirements.txt`
4. run the command  `image_check.py` 


    ```
    ./image_check.py --csv $CSVFILE.csv
    ```
The output will be dumped into a file `results.csv`

## Configuration

No configuration is needed, you can supply the follow flags during execution: 

| Option | Required | Description | 
| ---    | ---      | ---         |
| --csv  | Yes      | Supply the csv file to read the image list to compare. | 
| --log  | No       | Set the log level : Options = debug, error or info |
|--ext  | No       | Add supported image file extension (Must be supported by imagehash library) Example: '.png'|
| --help | No   | Displays Help Screen with Options | 

## How it works!

We just need to perform a simple operation. This Script works off the basis of being provided a .csv file and it does the following: 
1. Opens and reads each row.
2. Takes the first column image and compares it to the image in the second column. 
3. It uses the library [Imagehash](https://pypi.org/project/ImageHash/) to do the comparison. 
4. Stores the results into a dictionary then puts it into a list. 
5. Once complete running through each row we open the results file and run through the list dumping those dictionary entries as rows into the results file. 

## History

I've moved the image_check call to a function, so I can then import this as a module and use it in a flask app. The purpose of the flask app is to get over the issues of the user having to install anything. This could simple be hosted via docker in k8s or on a vm. 

## Logs

I've set the log format as `logformat` this way its easy to parse with solutions like ELK(ELF) or Datadog or anything else. I've also set it up with the intention that its verbose when running from the CLI or if running via kubernetes (This way you can easily pick up STDOUT from the pod). 

## Web App

This was quickly tossed together and could use some improvements, but this will allow the hosting of this app as a simple Flask app. All you would do is upload your file and it will return a file to download once it has completed the comparison. 

The logs don't currently output to the webpage but do on the web server. This is where some work is needed. 

Just simply run it as `./web.py` 

However it is recommended to put an auth proxy in front and probably an nginx proxy. 

**Right now there is no way to upload images so they need to be stored in the images directory provided in this repo, you can add then and make a merge request.**

## Module

How to use as a module: 
```
import image_check

image_check.compare_file(filename)
```

**compare_file**(filename, extensions)

    extensions: This is optional and default is = '.jpeg', '.jpg' and '.png'
    filename: Required and is the .csv file you want to check. 

**image_compare**(image_one, image_two)

    supply two image files (full path) to compare. 